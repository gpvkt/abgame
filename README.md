# A/B Game

Simple choice game.

## Demo

https://ab.21x9.org

## Installation

0. Install `python3` (and `pip` if needed)
1. Clone the Repo into `/opt/ab/`
2. Put A/B questions in `ab.txt`
3. Set config variables in `config.ini`
4. Install requirements (`pip install -r requirements.txt`)

## Run

### Manual

1. Run `chmod +x game.py`
2. Run `./game.py`

Or just run `python3 ./game.py`

### As Service

0. Install `supervisord`
1. Copy `supervisor.conf` to `/etc/supervisor/conf.d/abgame.conf`
2. Reload supervisor (`supervisorctl reload`)

## Use

Open your browser and navigate to `http://localhost:5000` or whatever you configured as `bind` and `port` in your `config.ini`.

## Reverse Proxy

To run this game in an production environment it's strongly recommended to set up an reverse proxy upfront.

### nginx

Make sure you understand the config and replace the `location` and/or `proxy_pass` value if necessary.

```nginx
location / {
        proxy_pass http://127.0.0.1:5000/;

        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

        client_max_body_size 32M;
        proxy_redirect off;
        proxy_intercept_errors off;
        proxy_read_timeout 86400s;
        proxy_ignore_client_abort on;
        proxy_connect_timeout 120s;

        proxy_buffer_size 128k;
        proxy_buffers 4 256k;
        proxy_busy_buffers_size 256k;

        proxy_headers_hash_max_size 512;
        proxy_buffering on;
        proxy_cache_bypass $http_pragma $http_authorization $cookie_nocache;
}
```
