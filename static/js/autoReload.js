function countdown() {
    var timeleft = 3;
    var reloadTimer = setInterval(function () {
        if (timeleft <= 0) {
            document.getElementById("next").innerHTML = 0;
            clearInterval(reloadTimer);
            window.location.replace('/')
        } else {
            document.getElementById("next").innerHTML = timeleft;
        }
        timeleft -= 1;
    }, 500);
}

function autoReload() {
    document.querySelectorAll('input[type="radio"]').forEach(function (el) {
        el.addEventListener('change', checkForm, { once: true });
    });

    function checkForm(e) {
        if (document.querySelectorAll('input[type="radio"]:checked').length == 5) {
            countdown();
        }
    }
}