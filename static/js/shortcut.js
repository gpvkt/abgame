var checkedGroups = [1,2,3,4,5];
var countdownActive = false;

function monitorRadio() {
    document.querySelectorAll('input[type="radio"]').forEach(function (el) {
        el.addEventListener('change', removeId, { once: true });
    });
}

function removeId(e) {
    if (e) {
        value = e.target.id.charAt(1);
    } else {
        value = checkedGroups[0];
    }
    checkedGroups = checkedGroups.filter(function(item) {
        return item != value;
    })
}

function checkCount() {
    if (checkedGroups.length == 0) {
        if (countdownActive == false) {
            countdown();
        }
        countdownActive = true;
        return true;
    }
    return false;
}

function check(choice) {
    value = checkedGroups[0];
    removeId();
    getid = choice+value;
    document.getElementById(getid).checked = true;
    checkCount();
}

window.addEventListener("keydown", function (event) {
    if (event.defaultPrevented) {
        return;
    }

    if (checkCount()) {
        return;
    };

    switch (event.key) {
        case "a":
            check("a");
            break;
        case "b":
            check("b");
            break;
        case "o":
            check("o");
            break;
        case "s":
            countdown();
            break;
        default:
            checkCount();
            return;
    }

    event.preventDefault();
}, true);
