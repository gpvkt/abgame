#! /bin/python3

"""
Simple A/B choice game
"""

from configparser import ConfigParser
from datetime import datetime
from email.message import EmailMessage
import smtplib
import random
import pytz
from flask import Flask, render_template, request

app = Flask(__name__,
            static_url_path='',
            static_folder='static',
            template_folder='templates')

config = ConfigParser()
config.read('config.ini')
tz = pytz.timezone(config.get('main', 'timezone'))

i18n = {
    'lang': config.get('i18n', 'lang'),
    'title': config.get('i18n', 'title'),
    'more': config.get('i18n', 'more'),
    'desc': config.get('i18n', 'desc'),
    'questions_prefix': config.get('i18n', 'questions_prefix'),
    'questions_suffix': config.get('i18n', 'questions_suffix'),
    'separator': config.get('i18n', 'separator'),
    'mailtext': config.get('i18n', 'mail_link'),
    'helptext': config.get('i18n', 'help_link'),
    'help': config.get('i18n', 'help'),
    'submit': config.get('i18n', 'submit'),
    'submit_questions': config.get('i18n', 'submit_questions'),
    'submit_thanks_title': config.get('i18n', 'submit_thanks_title'),
    'submit_thanks': config.get('i18n', 'submit_thanks')
}
conf = {
    'separator_char': config.get('main', 'separator_char'),
    'mailform': config.get('main', 'mailform').lower(),
    'mailserver': config.get('main', 'mailserver'),
    'mailto': config.get('main', 'mailto'),
    'mailfrom': config.get('main', 'mailfrom'),
    'mailsubject': config.get('main', 'mailsubject'),
    'url': config.get('main', 'base_url'),
    'theme': config.get('main', 'theme'),
    'animations': config.get('main', 'animations')
}

with open("ab.txt", "r", encoding="utf-8") as f:
    num_lines = sum(1 for _ in f)

def get_epoch():
    """
    Get current time as epoch timestamp
    """

    now = datetime.now(tz=tz)
    epoch = now.timestamp()
    epoch = int(epoch)

    return epoch

@app.errorhandler(404)
def page_not_found():
    """
    404 Error Page
    """
    epoch = get_epoch()
    return render_template('404.html', config=conf, i18n=i18n, epoch=epoch), 404

@app.errorhandler(500)
def internal_server_error():
    """
    500 Error Page
    """
    epoch = get_epoch()
    return render_template(
        '500.html', 
        config=conf,
        i18n=i18n,
        epoch=epoch
    ), 500

if conf['mailform'] == "true":
    @app.route("/form")
    def mailform():
        """
        Mail form
        """
        epoch = get_epoch()
        return render_template(
            'mailform.html',
            config=conf,
            i18n=i18n,
            epoch=epoch,
            num_lines=num_lines
        )

    @app.route("/send", methods=['POST', 'GET'])
    def sendmail():
        """
        Send form data via E-Mail
        """
        epoch = get_epoch()

        if request.method == 'POST':
            mailcontent = request.form['questions']
        else:
            mailcontent = request.args.get('questions')

        message = EmailMessage()
        message.set_content(mailcontent)
        message['Subject'] = conf['mailsubject']
        message['From'] = conf['mailfrom']
        message['To'] = conf['mailto']

        smtp_server = smtplib.SMTP(conf['mailserver'])
        smtp_server.send_message(message)
        smtp_server.quit()

        return render_template(
            'thanks.html',
            config=conf,
            i18n=i18n,
            epoch=epoch,
            questions=mailcontent
        )

@app.route("/")
def hello():
    """
    Default/Main page
    """
    ablines = []
    epoch = get_epoch()

    lines = get_content()
    while len(lines) < 2:
        print('Error reading content')
        print(lines)
        lines = get_content()

    for line in lines:
        abq = line.split(conf['separator_char'])
        ablines.append(
            {'A': str(abq[0]), 'B': str(abq[1])}
        )

    return render_template(
        'index.html',
        content=ablines,
        config=conf,
        i18n=i18n,
        num_lines=num_lines,
        epoch=epoch
    )

def get_content():
    """
    Read content from file
    """
    lines = [a.strip() for a in open("ab.txt", "r", encoding="utf-8").readlines()]
    result = random.sample(lines, 5)

    return result

app.register_error_handler(404, page_not_found)
app.register_error_handler(500, internal_server_error)

if __name__ == "__main__":
    from waitress import serve
    bind = config.get('main', 'bind')
    port = config.get('main', 'port')

    serve(app, host=bind, port=port, ident='a/b game')
